# Installing ``konrad``

## Previous steps

You need ``python``. Usually the pre-installed ``python`` distributions are not
enough: they are meant for some scripting in the operative system to work.
Instead, please install ``miniconda`` or ``anaconda``. Whereas ``anaconda``
install a complete software suite, I would recommend ``miniconda`` as it is a
light-weight installation.

Afterwards the installation, create a clean new environment with the most recent
``python`` version (larger or equal than 3.8).

You will also need `git` if you want to clone the repository, modify the code
and contribute. You can also obtain ``konrad`` using ``pip``.

### Specific to MacOS

You need first to install the Xcode developer tools, so that you can install
``brew`` as you need some compilers to build RRTMG.

Once ``brew`` is installed, use

```bash
brew install gcc@12;      # Installs GNU compilers in version 12
CC=gcc-12 FC=gfortran-12; # Correct values for the environment variables
```

If you have and	M1 Mac computer, then you need to change the target architecture
of the building process to ``ARMV8``. The following line does that 

```bash
[[ $(uname -p) == arm64 ]] && TARGET=ARMV8 || TARGET=HASWELL;
```

this line checks the architecture of your system and in case it is arm64, then
sets the correct target. In any other case, you have an Intel Macbook and the
architecture is set accordingly.

Finally, just before the ``konrad`` installation export the environment
variables

```bash
export CC FC TARGET;
```

## Installation

### ``conda`` environment

Create a new clean ``conda`` environment with ``python`` in a given version, for
example, using the following command

```bash
conda create --name konrad_env python=3.10;
```
Now, activate your environment and install some mandatory packages

```bash
conda install numpy scipy matplotlib netcdf4 xarray ipython!=8.7.0;
conda install -c rttools typhon pyarts;
```

### For MacOS only: Install RRTMG

After this and **if we are under MacOS**, we will install a skimmed version of
``CliMT`` a python package that contains the code of RRTMG, the radiative
transfer model that ``konrad`` uses. This skimmed version only contains the
RRTMG code, since other parts of CliMT can be difficult to compile in MacOS. For
Linux, this is not a problem and one can jump directly to the next section.
However, if you want to install the skimmed version of CliMT that compiles
faster than the full, follow this process except the selection of the target
architecture.

First, we set the correct environment variables once we have installed gcc-12
with ``brew``

```bash
CC=gcc-12 FC=gfortran-12;
[[ "$(uname -m)" == "arm64" ]] && TARGET=ARMV8 || TARGET=HASWELL;
export CC FC TARGET;
```

And then, we install the skimmed ``CliMT``
```bash
python -m pip install git+https://github.com/atmtools/climt@rrtmg-only
```
here it will build RRTMG, which is written in Fortran.

### Installing ``konrad`` by cloning the repository with ``git clone``

Now, clone ``konrad`` repository and change to its directory

```bash
git clone git@github.com:atmtools/konrad.git;
cd konrad;
```

and change to the branch you would like to install.

If you are on a Linux system, remember to set the correct environment variables
for the compilers

```bash
CC=gcc-12 FC=gfortran-12;
export CC FC;
```

and now issue the following command

```bash
python -m pip install -e .
```

the option ``-e`` allows to develop the code after installation (recommended).

The alternative is a static installation that does not allow changing code

```bash
python -m pip install .
```

If it finishes successfully the installation, you are ready to use ``konrad``!

### Installing ``konrad`` using the repository ``konrad-upstrato``

``konrad-upstrato`` is a version of ``konrad`` with certain extras for the
experiments for studying the role of upwelling and interactive ozone on the
tropical surface climate

If you want to install this version, follow the above-described process but
clone the ``konrad`` repository using the following command

```bash
git clone git@gitlab.dkrz.de:m300556/konrad-upstrato.git;
```

## ``konrad-up``

If you wish to repeat some of the experiments used for the paper where
``konrad-upstrato`` was used, then you can clone in another directory the
respository with the run scripts and the analysis tools.

The command for cloning the ``konrad-up`` repository is

```bash
git clone git@github.com:diegojco/konrad-up.git
```

In that repository under the ``experiment_list`` directory, you can find the
three files: ``conv-O3_last.py``, ``conv-O3_last.sh`` and
``experiment_list.txt``.

* ``conv-O3_last.py`` is an executable script that defines functions to run an
experiment from a toolbox of available options. The command line arguments are
fed to the functions and options such as the RH profile or ozone treatment are
switched. This is a good starting point for your own driving script

* ``conv-O3_last.sh`` is a proposal of ``bash`` running script for making
ensembles of runs from a list of experiments. This is mainly thought for the
HPC Levante and, therefore, it may be not relevant.

* ``experiment_list.txt`` is a list of experiments performed for the paper where
``konrad-upstrato`` was used. It contains the experiments in the notation
expected by the ``bash`` run script ``conv-O3_last.sh``.